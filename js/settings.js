$(document).ready(function() {

	$('.lift-top').offset({
		left: Math.floor($('.wrap').offset().left + $('.wrap').width() + 30)
	});

	$('.lift-top').click(function(e) {
		$('html, body').animate({
			scrollTop: 0
		}, 600);
	});

	(function(){
		var tabs = $('.tabs-container');

		if (tabs.length){
			var tabsBtn = tabs.find('.tabs-buttons').children();

			tabsBtn.eq(0).addClass('active');
			$('.tabs-content').children().hide();
			$('.tabs-content').children().eq(0).show();

			tabsBtn.on('click', function(e){
				var closestTabs;
				var tabsContent;

				e.preventDefault();
				$(this).addClass('active').siblings().removeClass('active');

				closestTabs = $(this).closest(tabs);
				tabsContent = closestTabs.find('.tabs-content').children();
				tabsContent.hide();
				tabsContent.eq($(this).index()).fadeIn(400);
			});
		}
	})();

	(function(){
		var toggleBtn = $('.toggle-control');

		toggleBtn.on('click', function(e){
			var container = $(this).closest('.toggle-container').find('.toggle-content');
			e.preventDefault();
			$(this).toggleClass('active');
			container.slideToggle(200);
		});
	})();


	(function ($) {
		$.support.placeholder = ('placeholder' in document.createElement('input'));
	})(jQuery);

	$(function () {
		if (!$.support.placeholder) {
			$("[placeholder]").focus(function () {
				if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
			}).blur(function () {
				if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
			}).blur();

			$("[placeholder]").parents("form").submit(function () {
				$(this).find('[placeholder]').each(function() {
					if ($(this).val() == $(this).attr("placeholder")) {
						$(this).val("");
					}
				});
			});
		}
	});

	$(window).scroll(function() {
		if ($(document).scrollTop() > 400 ) {
			$('.lift-top').addClass('visible');
		}
		else {
			$('.lift-top').removeClass('visible');
		}
	});


	function cartSum(){
		var cart = document.getElementById('cart-content');
		var content, cartTotal;

		if (cart){
			content = cart.getElementsByTagName('TBODY')[0];
			cartTotal = document.getElementById('total-cart');
			loadedContent();


			function loadedContent(){
				var rowPrice = null;
				var rowTotal = null;
				var rowQuant = null;
				var totalCart = 0;

				for(var i=0; i<content.children.length; i++){
					var row = content.children[i];

					rowPrice = +row.getElementsByClassName('prod-price')[0].innerHTML;
					rowQuant = +row.getElementsByClassName('quantity')[0].value;
					rowTotal = rowPrice*rowQuant;

					row.getElementsByClassName('prod-total')[0].innerHTML = rowTotal;
					totalCart += rowTotal;

					row.getElementsByClassName('quantity')[0].onkeypress = function(e){
						e = e || event;

						if (e.ctrlKey || e.altKey || e.metaKey) return;

						var chr = getChar(e);

						if (chr == null) return;

						if (chr < '0' || chr > '9')  {
							return false;
						}
					};

					row.getElementsByClassName('quantity')[0].onkeyup =function(e){
						var value = this.value;
						var row = null;

						if (value.charAt(0) == '0'){
							this.value = value.split('').slice(1).join('');
						}

						row = this.parentNode;
						while (row.tagName != 'TR'){
							row = row.parentNode;
						}

						rowValue(row);
					};
				}

				cartTotal.innerHTML = totalCart;

			}

			function rowValue(row){
				var oldValue = +row.getElementsByClassName('prod-total')[0].innerHTML;
				var newValue = +row.getElementsByClassName('quantity')[0].value * +row.getElementsByClassName('prod-price')[0].innerHTML;

				row.getElementsByClassName('prod-total')[0].innerHTML = newValue;
				newValue > oldValue ? cartTotal.innerHTML = +cartTotal.innerHTML + (newValue - oldValue) : 
				cartTotal.innerHTML = +cartTotal.innerHTML - (oldValue - newValue);

				return +row.getElementsByClassName('prod-total')[0].innerHTML;
			}

			function getChar(event) {
				if (event.which!=0 && event.charCode!=0) {
					if (event.which < 32) return null;
					return String.fromCharCode(event.which)
				}
				return null;
			}

			cart.onclick = function(e){
				var target = e.target;
				var row, rowPrice = null;
				var totalCart = +cartTotal.innerHTML;

				if ((target.className == 'delete') || target.hasAttribute('data-quantity')){
					row = target.parentNode;
					while (row.tagName != 'TR'){
						row = row.parentNode;
					}

					if (target.className == 'delete'){
						rowPrice = +row.getElementsByClassName('prod-total')[0].innerHTML;
						cartTotal.innerHTML = totalCart - rowPrice;
						content.removeChild(row);
						return;
					}

					if (target.getAttribute('data-quantity') == 'plus'){
						target.previousElementSibling.value++;
						rowValue(row);
					}

					if (target.getAttribute('data-quantity') == 'minus'){
						if (+target.nextElementSibling.value > 1){
							target.nextElementSibling.value--;
							rowValue(row);
						}
						else{
							cartTotal.innerHTML = +cartTotal.innerHTML - rowValue(row);
							content.removeChild(row);
						}
					}

					target.onmousedown = target.onselectstart = function() {
						return false;
					}
				}
			}
		}
	}

	cartSum();

});